//
//  Constants.swift
//  UniNet
//
//  Created by Anton Debelyy on 10.02.2023.
//

import UIKit

struct Constants {
    static let cornerRadius: CGFloat = 8.0
}
