//
//  File.swift
//  UniNet
//
//  Created by Anton Debelyy on 10.02.2023.
//

import Foundation

extension String {
    func safeDatabaseKey() -> String {
        return self.replacingOccurrences(of: "@", with: "-").replacingOccurrences(of: ".", with: "-")
    }
}
