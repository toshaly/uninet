//
//  MainTabBarController.swift
//  UniNet
//
//  Created by Anton Debelyy on 09.02.2023.
//

import UIKit
import FirebaseAuth

class MainTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .systemPurple
        self.setupVCs()
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.handleNotAuth()
        print("current usr is \(Auth.auth().currentUser)")
        
    }
    
    private func handleNotAuth() {
        // check auth status
        if Auth.auth().currentUser == nil {
            let loginVC = LoginViewController()
            loginVC.modalPresentationStyle = .fullScreen
            self.present(loginVC, animated: true)
        }
    }
    
    fileprivate func createNavController(for rootViewController: UIViewController,
                                                      title: String,
                                                      image: UIImage) -> UIViewController {
            let navController = UINavigationController(rootViewController: rootViewController)
            navController.tabBarItem.title = title
            navController.tabBarItem.image = image
            navController.navigationBar.prefersLargeTitles = true
            rootViewController.navigationItem.title = title
            return navController
        }
    
    
    func setupVCs() {
          viewControllers = [
            createNavController(for: HomeViewController(),
                                title: NSLocalizedString("Home", comment: ""),
                                image: UIImage(systemName: "house") ?? UIImage()),
            createNavController(for: ProfileViewController(),
                                  title: NSLocalizedString("Profile", comment: ""),
                                  image: UIImage(systemName: "person.circle" ) ?? UIImage()),
            createNavController(for: ExploreViewController(),
                                title: NSLocalizedString("Explore", comment: ""),
                                image: UIImage(systemName: "globe.europe.africa" ) ?? UIImage()),
            createNavController(for: NotificationViewController(),
                                title: NSLocalizedString("Notifications", comment: ""),
                                image: UIImage(systemName: "bell.circle") ?? UIImage()),
            createNavController(for: CameraViewController(),
                                title: "Camera",
                                image: UIImage(systemName: "camera.circle") ?? UIImage())
        
          ]
      }
      

}
