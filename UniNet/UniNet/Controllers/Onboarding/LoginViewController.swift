//
//  LoginViewController.swift
//  UniNet
//
//  Created by Anton Debelyy on 09.02.2023.
//
import SafariServices
import FirebaseAuth
import UIKit

class LoginViewController: UIViewController{
    
    private let usernameEmailField: UITextField = {
        let field = UITextField()
        field.placeholder = "Username or email"
        field.returnKeyType = .next
        field.leftViewMode = .always
        field.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 0))
        field.autocapitalizationType = .none
        field.autocorrectionType = .no
        field.layer.masksToBounds = true
        field.layer.cornerRadius = Constants.cornerRadius
        field.backgroundColor = .secondarySystemBackground
        field.layer.borderWidth = 1.0
        field.layer.borderColor = UIColor.secondaryLabel.cgColor
        return field
    } ()
    
    private let passwordField: UITextField = {
        let field = UITextField()
        field.placeholder = "Password"
        field.returnKeyType = .continue
        field.leftViewMode = .always
        field.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 0))
        field.autocapitalizationType = .none
        field.autocorrectionType = .no
        field.layer.masksToBounds = true
        field.layer.cornerRadius = Constants.cornerRadius
        field.isSecureTextEntry = true
        field.backgroundColor = .secondarySystemBackground
        field.layer.borderWidth = 1.0
        field.layer.borderColor = UIColor.secondaryLabel.cgColor
        return field
    } ()
    
    private let loginButton: UIButton = {
        let button = UIButton()
        button.setTitle("Log IN", for: .normal)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = Constants.cornerRadius
        button.backgroundColor = .systemBlue
        button.addTarget(self, action: #selector(didTapLoginButton), for: .touchUpInside)
        button.setTitleColor(.white, for: .normal)
        return button
    } ()
    
    private let privacyButton: UIButton = {
        let button = UIButton()
        button.setTitle("Terms of service", for: .normal)
        button.setTitleColor(.secondaryLabel, for: .normal)
        button.addTarget(self, action: #selector(didTapPrivacyButton), for: .touchUpInside)
        return button
    } ()
    
    private let createAccButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.label, for: .normal)
        button.setTitle("New User? Create an Account", for: .normal)
        button.addTarget(self, action: #selector(didTapCreateAccButton), for: .touchUpInside)
        return button
    } ()
    
    private let termsButton: UIButton = {
        let button = UIButton()
        button.setTitle("Privacy Policy", for: .normal)
        button.setTitleColor(.secondaryLabel, for: .normal)
        button.addTarget(self, action: #selector(didTapTermButton), for: .touchUpInside)
        return button
    } ()
    
    private let headerView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        let background = UIImageView(image: UIImage(named: "gradback"))
        background.layer.cornerRadius = Constants.cornerRadius
        view.addSubview(background)
        return view
    } ()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .systemBackground
        self.usernameEmailField.delegate = self
        self.passwordField.delegate = self
        self.addSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //assign frames
        self.headerView.frame = CGRect(
            x: 0,
            y: 0,
            width: self.view.width,
            height: view.height/3.0)
        
        self.usernameEmailField.frame = CGRect(
            x: 25,
            y: headerView.bottom + 40,
            width: self.view.width - 50,
            height: 52)
        
        self.passwordField.frame = CGRect(
            x: 25,
            y: usernameEmailField.bottom + 10,
            width: self.view.width - 50,
            height: 52)
        
        self.loginButton.frame = CGRect(
            x: 25,
            y: passwordField.bottom + 10,
            width: self.view.width - 50,
            height: 52)
        
        self.createAccButton.frame = CGRect(
            x: 25,
            y: loginButton.bottom + 10,
            width: self.view.width - 50,
            height: 52)
        
        self.termsButton.frame = CGRect(
            x: 10,
            y: view.height - view.safeAreaInsets.bottom - 100,
            width: view.width - 20,
            height: 50)
        
        self.privacyButton.frame = CGRect(
            x: 10,
            y: view.height - view.safeAreaInsets.bottom - 50,
            width: view.width - 20,
            height: 50)
        
        self.configureHeaderView()
    }
    
    
    private func addSubviews() {
        self.view.addSubview(usernameEmailField)
        self.view.addSubview(passwordField)
        self.view.addSubview(loginButton)
        self.view.addSubview(termsButton)
        self.view.addSubview(privacyButton)
        self.view.addSubview(createAccButton)
        self.view.addSubview(headerView)
    }
    
    private func configureHeaderView() {
        guard self.headerView.subviews.count == 1 else { return }
        guard let background = self.headerView.subviews.first else { return }
        background.frame = headerView.bounds
        
        let image = UIImageView(image: UIImage(named: "text"))
        headerView.addSubview(image)
        image.contentMode = .scaleAspectFit
        image.frame = CGRect(x: headerView.width/4.0, y: view.safeAreaInsets.top, width: headerView.width/2.0, height: headerView.height - view.safeAreaInsets.top)
    }
    
    @objc private func didTapLoginButton() {
        self.passwordField.resignFirstResponder()
        self.usernameEmailField.resignFirstResponder()
        guard let userEmail = usernameEmailField.text, !userEmail.isEmpty,
              let password = passwordField.text, !password.isEmpty, password.count >= 8 else { return }
        
        //login functionality
        var username: String?
        var email: String?

        if userEmail.contains("@"), userEmail.contains(".") {
            email = userEmail
        }
        else {
            username = userEmail
        }
        AuthManager.shared.loginUser(username: username, email: email, password: password) { success in
            DispatchQueue.main.async {
                if success {
                    self.dismiss(animated: true, completion: nil)
                }
                else {
                    let alert = UIAlertController(title: "LogIn Error",
                                                  message: "We cann't to log you in",
                                                  preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                }
            }
        }
        
    }
    
    @objc private func didTapTermButton() {
        guard let url = URL(string: "https://vk.com/terms") else { return }
        let vc = SFSafariViewController(url: url)
        present(vc, animated: true)
    }
    
    @objc private func didTapPrivacyButton() {
        guard let url = URL(string: "https://vk.com/privacy") else { return }
        let vc = SFSafariViewController(url: url)
        present(vc, animated: true)
    }
    
    @objc private func didTapCreateAccButton() {
        let vc = RegistrationViewController()
        vc.title = "Create Account"
        present(UINavigationController(rootViewController: vc), animated: true)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == usernameEmailField {
            passwordField.becomeFirstResponder()
        }
        else if textField == passwordField {
            didTapLoginButton()
        }
        return true
    }
}
